import GraphWrapper from "./GraphWrapper";
import loadGraph from "./loadGraph";

async function getGraph(name){
  const graph = new GraphWrapper();
  const edges = await loadGraph(name);
  edges.forEach((val) => {
    graph.addEdge(val[0], val[1], val[2]);
  });
  return graph;
}

export default getGraph;