import getGraph from "./getGraph";
import * as graphlib from "graphlib";
import * as graphix from "graphix";
const Benchmark = require('benchmark');


async function main(){
  const suite = new Benchmark.Suite;

  const g = await getGraph('USA-road-d.NY.gr');
  const graphlib_NY = g.graphlib;
  const graphix_NY = g.graphix;

  console.log("ready");

  suite
    .add('USA-road-NY Graphlib', function() {
      graphlib.alg.dijkstra(graphlib_NY, 1, (e) => graphlib_NY.edge(e));
    })
    .add('USA-road-NY Graphix', function(deferred) {
      graphix.dijkstraAll(graphix_NY, 1).then((res) => {
        deferred.resolve();
      });
    }, {'defer': true})
    .on('cycle', function(event) {
      console.log(String(event.target));
    })
    .on('complete', function() {
      console.log('Fastest is ' + this.filter('fastest').map('name'));
    })
    .run({ 'async': true });
}

main();