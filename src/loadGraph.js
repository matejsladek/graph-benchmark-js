const readline = require('readline');
const fs = require('fs');

function loadRoadGraph(name){
  return new Promise((resolve) => {
    const edges = [];
    const rl = readline.createInterface({
      input: fs.createReadStream(`data/${name}`)
    });

    rl.on('line', (line) => {
      const tokens = line.split(' ');
      const numbers = tokens.filter((val, key) => key !== 0).map(e => Number(e));
      if(tokens[0] === 'a'){
        edges.push(numbers);
      }
    }).on('close', () => {
      console.log('Have a great day!');
      resolve(edges);
    });
  });
}

function loadGraph(name){
  if(name.startsWith('USA-road-d.')) return loadRoadGraph(name);
  return [];
}

export default loadGraph;