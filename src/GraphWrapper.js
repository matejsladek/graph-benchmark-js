import * as graphix from 'graphix'
import * as graphlib from 'graphlib'

class GraphWrapper{
  constructor(){
    this.graphlib = new graphlib.Graph();
    this.graphix = new graphix.Graph();
  }

  addEdge(u, v, w){
    this.graphlib.setEdge(u, v, w);
    this.graphix.addEdge(u, v, w);
  }
}

export default GraphWrapper;